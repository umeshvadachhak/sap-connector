package com.appnomic.appsone.connectors.utill;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

public class ConfigDataReader {
    HttpsURLConnection conn;
    String requestUrl;
    private static final Logger log = Logger.getLogger(ConfigDataReader.class);
    public String sendDataToDB(List<Object> transactionList) throws IOException, ParseException {
        requestUrl = "https://" + ConfigInitializer.DS_IP + ":" + ConfigInitializer.DS_PORT + ConfigInitializer.DS_TRAN_URL_ENDPOINT;
//       log.info("requestUrl :"+requestUrl);
        String authToken = GenerateAuthToken.generateAuthToken();
//       log.info("authToken :"+authToken);
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
//       log.info("getSocketFactory() :"+sc.getSocketFactory());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        URL url = null;
        JSONObject jsonObj = null;
        int responseCode = 0;
        StringBuilder response = null;
        try{
            url = new URL(requestUrl);
           log.info("Sending 'POST' request to URL : " + url);
            conn = (HttpsURLConnection) url.openConnection();
           log.info("Connection : "+conn);
            conn.setReadTimeout(Integer.parseInt(ConfigInitializer.READ_TIME_OUT));
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", authToken);

            // For POST only - START
            byte[] sendData = transactionList.toString().getBytes();
           //log.info("Send data : "+ Arrays.toString(sendData));
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            os.write(sendData);
            os.flush();
            os.close();
            // For POST only - END
            responseCode = conn.getResponseCode();

           log.info("Response Code : " + responseCode);
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
               log.info("response : "+response.toString());


        }catch(Exception e){
            e.printStackTrace();
        }
        if (response!=null) {
            JSONObject returnMessage = (JSONObject) new JSONParser().parse(response.toString());
            return returnMessage.get("responseStatus").toString();
        }
        return "ERROR";

    }

    public void sendRequest() throws IOException {

        requestUrl = "https://" + ConfigInitializer.DS_IP + ":" + ConfigInitializer.DS_PORT + ConfigInitializer.DS_URL_ENDPOINT;
        String authToken = GenerateAuthToken.generateAuthToken();
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        URL url = null;
        JSONObject jsonObj = null;
        try {

            url = new URL(requestUrl);
           log.info("Sending 'GET' request to URL : " + url);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(60000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", authToken);
            int responseCode = conn.getResponseCode();
           log.info("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            jsonObj = (JSONObject) parseJson(response.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        createFile(jsonObj);
    }

    private Object parseJson(String jsonString) {
        JSONParser jsonParser = new JSONParser();
        Object json = null;
        try {
            json = jsonParser.parse(jsonString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return json;
    }

    private void createFile(JSONObject originalJsonObject) {
        if (!Files.exists(Paths.get(ConfigInitializer.DS_FILE_NAME)))
            try {
                Files.createFile(Paths.get(ConfigInitializer.DS_FILE_NAME));
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        try {
            FileUtils.writeStringToFile(new File(ConfigInitializer.DS_FILE_NAME), "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        String originalJson = originalJsonObject.toJSONString();
        JsonNode tree;
        try {
            tree = objectMapper.readTree(originalJson);
            String formattedJson = objectMapper.writeValueAsString(tree);
            writeStringToFile(formattedJson, ConfigInitializer.DS_FILE_NAME, false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void writeStringToFile(String data, String destFile, boolean append) {
        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        try {
            fileWriter = new FileWriter(destFile, append);
            printWriter = new PrintWriter(fileWriter);
            printWriter.println(data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
                printWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }




}
