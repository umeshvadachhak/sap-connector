package com.appnomic.appsone.connectors.utill;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ConfigDSJsonParser {
	HashMap<String, Object> singleInstance;
	private static final Logger log = LoggerFactory.getLogger(ConfigDSJsonParser.class);

	public HashMap<String, Object> getKpiDetails(String accountId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		JSONObject jsonObject = getSingleAccountJson(parseJsonFile(), accountId);

		for (int i = 0; i < ((JSONArray) jsonObject.get("agentList")).size(); i++) {
			ArrayList<String> compInstanceIds = new ArrayList<String>();
			if (((JSONObject) ((JSONArray) jsonObject.get("agentList")).get(i)).get("agentTypeName").toString()
					.equalsIgnoreCase("ComponentAgent")) {
				String id = "" + ((JSONObject) ((JSONArray) jsonObject.get("agentList")).get(i)).get("agentId");
				for (int j = 0; j < ((JSONArray) ((JSONObject) ((JSONArray) jsonObject.get("agentList")).get(i))
						.get("compInstanceIds")).size(); j++) {
					compInstanceIds
							.add("" + ((JSONArray) ((JSONObject) ((JSONArray) jsonObject.get("agentList")).get(i))
									.get("compInstanceIds")).get(j));

				}
				singleInstance = new HashMap<String, Object>();

				for (int k = 0; k < ((JSONArray) jsonObject.get("compInstanceDetail")).size(); k++) {
					if (compInstanceIds
							.contains(((JSONObject) ((JSONArray) jsonObject.get("compInstanceDetail")).get(k))
									.get("instanceId").toString())) {
						ArrayList<HashMap<String, Object>> kpis = new ArrayList<HashMap<String, Object>>();
						for (int l = 0; l < ((JSONArray) ((JSONObject) ((JSONArray) jsonObject
								.get("compInstanceDetail")).get(k)).get("kpis")).size(); l++) {
							HashMap<String, Object> kpi = new HashMap<String, Object>();
							if (!(boolean) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) jsonObject
									.get("compInstanceDetail")).get(k)).get("kpis")).get(l)).get("isGroup")) {
								kpi.put("kpiType",
										((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) jsonObject
												.get("compInstanceDetail")).get(k)).get("kpis")).get(l))
														.get("kpiType"));
								kpi.put("isKpiGroup", false);
								kpi.put("kpiUid", ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) jsonObject
										.get("compInstanceDetail")).get(k)).get("kpis")).get(l)).get("id"));
								kpi.put("kpiName", ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) jsonObject
										.get("compInstanceDetail")).get(k)).get("kpis")).get(l)).get("kpiAliasName"));
								kpi.put("collectionInterval",
										((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) jsonObject
												.get("compInstanceDetail")).get(k)).get("kpis")).get(l))
														.get("collectionInterval"));
								kpis.add(kpi);

							}

						}

						singleInstance.put(((JSONObject) ((JSONArray) jsonObject.get("compInstanceDetail")).get(k))
								.get("identifier").toString(), kpis);
					}

				}
				map.put(id, singleInstance);
			}
		}
		return map;

	}

	public HashMap<String, Object> getTxnDetails(String accountId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// JSONObject jsonObject = getSingleAccountJson(parseJsonFile(), accountId);
		return map;
	}

	private Object parseJsonFile() {
		JSONParser jsonParser = new JSONParser();
		Object json = null;
		try {
			FileReader fr = new FileReader(ConfigInitializer.DS_FILE_NAME);
			json = jsonParser.parse(fr);
			fr.close();
		} catch (ParseException e) {
			log.error(e.toString());
		} catch (FileNotFoundException e) {
			log.error(e.toString());

		} catch (IOException e) {
			log.error(e.toString());
		}
		return json;
	}

	private JSONObject getSingleAccountJson(Object obj, String accountId) {
		JSONObject jsonObject = (JSONObject) obj;
		for (int i = 0; i < ((JSONArray) jsonObject.get("result")).size(); i++) {
			if (((JSONObject) ((JSONArray) jsonObject.get("result")).get(i)).get("accountId").toString()
					.equalsIgnoreCase(accountId)) {
				return ((JSONObject) ((JSONArray) jsonObject.get("result")).get(i));

			}
		}
		return null;
	}

}
