package com.appnomic.appsone.connectors.utill;

import com.appnomic.appsone.connectors.sapConnector.SAPClient;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class JSONDataParser {

    static final Logger logger = Logger.getLogger(JSONDataParser.class);

    public Map<String,JSONObject> getTransactionDetails(List<Map<String, String>> data,String instance){
        String transIdValue = null;
        Map<String,JSONObject> jsonList = new HashMap<>();
        JSONObject jsonFortransaction = null;
//        JSONArray fileJsonData =  getDiscoveryJSONFile();

        for (Map<String,String> map:data) {

            String transactionIdentifier = map.get("Transaction Identifier");
            String transctionId = map.get("Transaction ID");

            logger.info("verifying transaction :"+transactionIdentifier);

            JSONObject transactionIdentifierForDiscovery = new JSONObject();

            transactionIdentifierForDiscovery.put("transId",transactionIdentifier);

            logger.info(transactionIdentifierForDiscovery);

            if (!(SAPClient.discoveryList.contains(transactionIdentifierForDiscovery))){

                jsonFortransaction = getTransactionJson(parseJsonFileForInstance(instance),transctionId,transactionIdentifier);
                jsonList.put(transactionIdentifier,jsonFortransaction);

            }
        }


        return jsonList;
    }

    public static void addIntoFile(List<JSONObject> object){
        JSONParser jsonParser = new JSONParser();
        try {
            FileReader reader = new FileReader(ConfigInitializer.DISCOVERY_FILE);
            Object obj = jsonParser.parse(reader);
            reader.close();
            JSONArray jsonArray = (JSONArray)obj;
            for (JSONObject o:object) {
                jsonArray.add(o);
            }
            FileWriter file = new FileWriter(ConfigInitializer.DISCOVERY_FILE);
            file.write(jsonArray.toJSONString());
            file.flush();
            file.close();
        } catch (ParseException e) {
            logger.error("Getting error when adding this json object : "+object);
            logger.error(e);
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            logger.error("Getting error when adding this json object : "+object);
            logger.error(e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Getting error when adding this json object : "+object);
            logger.error(e);
            e.printStackTrace();
        }
    }


    public JSONArray getDiscoveryJSONFile() {
        JSONParser jsonParser = new JSONParser();
        JSONArray json = null;
        try {
            logger.info("file strea before");
            logger.info(ConfigInitializer.DISCOVERY_FILE);
            FileReader fr = new FileReader(ConfigInitializer.DISCOVERY_FILE);
            logger.info("file strea :"+fr.toString());
            json = (JSONArray) jsonParser.parse(fr);
//            System.out.println("JSON DATA :"+json);
            fr.close();
        } catch (ParseException e) {
            logger.error("Getting error when reading discovery file : ",e);
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            logger.error("Getting error when reading discovery file : ",e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Getting error when reading discovery file : ",e);
            e.printStackTrace();
        }
        return json;
    }

    private JSONObject getTransactionJson(Object obj, String transId,String transactionIdentifier ) {
        JSONObject jsonObject = (JSONObject) obj;
        for(Iterator iterator = jsonObject.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
           // System.out.println(jsonObject.get(key));
            if(key.equals("txnName")){
                jsonObject.put("txnName",transId);
            }
            if(key.equals("description")){
                jsonObject.put("description",transId);
            }
            if(key.equals("txnIdentifier")){
                jsonObject.put("txnIdentifier",transactionIdentifier);
            }
        }
        return jsonObject;
    }

    private Object parseJsonFileForInstance(String instance) {
        JSONParser jsonParser = new JSONParser();
        Object json = null;
        try {
            FileReader fr = new FileReader(ConfigInitializer.INSTANCE_MAPPING.get(instance).get("txnFile"));
            json = jsonParser.parse(fr);
            //System.out.println("JSON :"+json);
            fr.close();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }
}
