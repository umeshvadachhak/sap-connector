package com.appnomic.appsone.connectors.utill;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
public class GenerateAuthToken {
    private static  final Logger logger = Logger.getLogger(GenerateAuthToken.class);
    public static String generateAuthToken() throws IOException {
        String authToken = "";
        String[] args = {"/usr/bin/curl" ,"-s", "-X" ,"POST" ,"--insecure" ,"https://"+ConfigInitializer.DS_IP+":"+ConfigInitializer.DS_PORT+"/auth/realms/master/protocol/openid-connect/token" ,"-H" ,"Content-Type=application/x-www-form-urlencoded" ,"-d" ,"username=appsoneadmin" ,"-d" ,"password=Appsone@123" ,"-d" ,"client_id=admin-cli" ,"-d" ,"grant_type=password"};
        final StringBuffer sb = new StringBuffer();
        int processComplete = -1;
        ProcessBuilder pb = new ProcessBuilder(args);
        pb.redirectErrorStream(true);
        try {
            final Process process = pb.start();
            final InputStream is = process.getInputStream();
            // the background thread watches the output from the process
            new Thread(new Runnable() {
                public void run() {
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line).append('\n');
                        }
                    } catch (IOException e) {
                        logger.error("Java ProcessBuilder: IOException occured.");
                        logger.error(e.getMessage());
                    } finally {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
            // Wait to get exit value
            // the outer thread waits for the process to finish
            processComplete = process.waitFor();
            //System.out.println("Java ProcessBuilder result:" + processComplete);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println("Java ProcessBuilder - return=: " + sb.toString());
        JSONObject jsonObj = (JSONObject) parseJson(sb.toString());
        //System.out.println("JsonObject : "+jsonObj);
        if (jsonObj != null) {
            authToken = jsonObj.get("access_token").toString();
            logger.info("Auth token : "+authToken);
        }
        return authToken;
    }
    private static Object parseJson(String jsonString) {
        JSONParser jsonParser = new JSONParser();
        Object json = null;
        try {
            json = jsonParser.parse(jsonString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return json;
    }
}
