package com.appnomic.appsone.connectors.utill;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ConfigInitializer {
    private static final Logger log = LoggerFactory.getLogger(ConfigInitializer.class);
    public static String SSL_VERSION,FORMAT_TIMESTAMP,ACCOUNTID, GRPC_PORT, DS_IP, DS_PORT,DS_URL_ENDPOINT,
            DS_FILE_NAME,HOST_NAME,READ_TIME_OUT,DISCOVERY_FILE,DS_TRAN_URL_ENDPOINT,
            DS_TRAN_FILE_NAME,ENTERPRISENAME,LOG_FILE,TASK_FILE,TIME_ZONE,TIMESTAMP,DESTINATION_FILE,MODE;
    public static int CHUNK_SIZE ,RULE_ID,FETCH_INTEVAL;

    public static boolean USE_SSL_PROTOCOL;
    public static Map<String,Map<String,String>> INSTANCE_MAPPING = new LinkedHashMap<>();
    public static List<Map<String,String>> FUNCTIONAL_MODULES = new ArrayList<>();
    public static Map<String,String> TIMESTAMP_MAPPING = new LinkedHashMap<>();

    public static void getParametersFromConfigFile(Object json) {
        JSONObject jsonObject = (JSONObject) json;
        LOG_FILE = (String) jsonObject.get("logProprties");
        TIME_ZONE = (String) jsonObject.get("timeZone");
        TIMESTAMP = (String) jsonObject.get("timeStamp");
        DESTINATION_FILE = (String) jsonObject.get("destinationFile");
        MODE = (String) jsonObject.get("mode");
        TASK_FILE = (String) jsonObject.get("tasktypeFile");
        READ_TIME_OUT=(String)jsonObject.get("readTimeOut");
        ACCOUNTID = (String) jsonObject.get("accountId");
        GRPC_PORT = (String) jsonObject.get("grpcPort");
        DS_IP = (String) jsonObject.get("configDSIp");
        DS_PORT = (String) jsonObject.get("configDSPort");
        DS_URL_ENDPOINT=(String)jsonObject.get("dsUrlEndpoint");
        DS_FILE_NAME=(String)jsonObject.get("dsFileName");
        HOST_NAME=(String)jsonObject.get("hostName");
        CHUNK_SIZE=Integer.parseInt(jsonObject.get("chunkSize").toString());
        FETCH_INTEVAL = Integer.valueOf (jsonObject.get("fetchInteval").toString());
        FORMAT_TIMESTAMP=jsonObject.get("formatOfTimestamp").toString();
        USE_SSL_PROTOCOL=(Boolean)jsonObject.get("useSSLProtocol");
        SSL_VERSION=(String) jsonObject.get("sslVersion");
        DISCOVERY_FILE=(String)jsonObject.get("discoverFileName");
        DS_TRAN_URL_ENDPOINT = (String)jsonObject.get("dsTranUrlEndpoint");
        DS_TRAN_FILE_NAME = (String)jsonObject.get("dsTranFileName");
        JSONArray array = (JSONArray) jsonObject.get("instanceMapping");
        for (int i = 0; i < array.size(); i++) {
            Map<String,String> map = ConfigInitializer.toMap((JSONObject) array.get(i));
            Map<String,String> dataMap = new HashMap<>();
            dataMap.putAll(map);
            dataMap.remove("SAPinstance");
            INSTANCE_MAPPING.put(map.get("SAPinstance"),dataMap);
        }
        JSONArray modulearray = (JSONArray) jsonObject.get("functionlModules");
        for (int i = 0; i < modulearray.size(); i++) {
            Map<String,String> map = ConfigInitializer.toMap((JSONObject) modulearray.get(i));
            FUNCTIONAL_MODULES.add(map);
        }
        TIMESTAMP_MAPPING = toMap((JSONObject)jsonObject.get("timeStampMap"));
    }

    public static Map<String, String> toMap(JSONObject object) {
        Map<String, String> map = new HashMap<>();

        Set<String> keysSet = object.keySet();
        keysSet.forEach(key ->{
            String value=(String)object.get(key);
            map.put(key,value);
        });
        return map;
    }

    public static Map<String,String> parseConfigFile(String configFile) {
        JSONParser jsonParser = new JSONParser();
        Object json = null;
        Map<String, String> transactionMappings = null;
        try {
            FileReader fr = new FileReader(configFile);
            json = jsonParser.parse(fr);
            getParametersFromConfigFile(json);
            fr.close();
        } catch (ParseException e) {
            log.error(e.toString());
        } catch (FileNotFoundException e) {
            log.error(e.toString());

        } catch (IOException e) {
            log.error(e.toString());
        } finally {
            return transactionMappings;
        }
    }
}
