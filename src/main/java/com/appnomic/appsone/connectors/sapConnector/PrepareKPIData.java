package com.appnomic.appsone.connectors.sapConnector;

import com.appnomic.appsone.common.protbuf.KPIAgentMessageProtos;
import com.appnomic.appsone.common.protbuf.KPIAgentMessageProtos.KPIAgentMessage.KpiData.KpiType;

import java.util.HashMap;

public class PrepareKPIData {

	@SuppressWarnings("unchecked")
	public KPIAgentMessageProtos.KPIAgentMessage.KpiData getKpiData(Object object,String starttime) {
		HashMap<String, Object> params = (HashMap<String, Object>) object;
		HashMap<String, String> groupKpiPairs = new HashMap<String, String>();
		if (params.containsKey("groupKpiPairs") && params.get("groupKpiPairs") != null) {
			for (String s : ((String) params.get("groupKpiPairs")).split("::")) {
				groupKpiPairs.put(s.split("\\$")[0], s.split("\\$")[1]);
			}
		}
		KpiType kpiType = null;
		KPIAgentMessageProtos.KPIAgentMessage.KpiData kpi = null;
		switch ((String) params.get("kpiType")) {
		case "Core":
			kpiType = KpiType.Core;
			break;
		case "Availability":
			kpiType = KpiType.Availability;
			break;
		case "ComputedAvailability":
			kpiType = KpiType.ComputedAvailability;
			break;
		case "ConfigWatch":
			kpiType = KpiType.ConfigWatch;
			break;
		case "FileWatch":
			kpiType = KpiType.FileWatch;
			break;
		case "Forensic":
			kpiType = KpiType.Forensic;
			break;
		}
		if ((boolean) params.get("isKpiGroup")) {
			if (params.containsKey("val") && params.get("val") != null) {
				kpi = KPIAgentMessageProtos.KPIAgentMessage.KpiData.newBuilder()
						.setKpiUid(Integer.parseInt(params.get("kpiUid") + "")).setTimeInGMT(starttime)
						.setKpiType(kpiType).setKpiName((String) params.get("kpiName"))
						.setVal((String) params.get("val")).setKpiGroupName((String) params.get("KpiGroupName"))
						.setGroupKpi(KPIAgentMessageProtos.KPIAgentMessage.KpiData.GroupKpi.newBuilder()
								.putAllPairs(groupKpiPairs).build())
						.setIsKpiGroup(true)
						.setCollectionInterval(Integer.parseInt(params.get("collectionInterval") + "")).build();
			} else {
				kpi = KPIAgentMessageProtos.KPIAgentMessage.KpiData.newBuilder()
						.setKpiUid(Integer.parseInt(params.get("kpiUid") + "")).setTimeInGMT(starttime)
						.setKpiType(kpiType).setKpiName((String) params.get("kpiName"))
						.setKpiGroupName((String) params.get("KpiGroupName"))
						.setGroupKpi(KPIAgentMessageProtos.KPIAgentMessage.KpiData.GroupKpi.newBuilder()
								.putAllPairs(groupKpiPairs).build())
						.setIsKpiGroup(true)
						.setCollectionInterval(Integer.parseInt(params.get("collectionInterval") + "")).build();
			}
		} else {
			if (params.containsKey("val") && params.get("val") != null) {
				kpi = KPIAgentMessageProtos.KPIAgentMessage.KpiData.newBuilder()
						.setKpiUid(Integer.parseInt(params.get("kpiUid") + "")).setTimeInGMT(starttime)
						.setKpiType(kpiType).setKpiName((String) params.get("kpiName"))
						.setVal((String) params.get("val")).setIsKpiGroup(false)
						.setCollectionInterval(Integer.parseInt(params.get("collectionInterval") + "")).build();
			} else {
				kpi = KPIAgentMessageProtos.KPIAgentMessage.KpiData.newBuilder()
						.setKpiUid(Integer.parseInt(params.get("kpiUid") + "")).setTimeInGMT(starttime)
						.setKpiType(kpiType).setKpiName((String) params.get("kpiName")).setIsKpiGroup(false)
						.setCollectionInterval(Integer.parseInt(params.get("collectionInterval") + "")).build();
			}
		}
		return kpi;
	}
}
