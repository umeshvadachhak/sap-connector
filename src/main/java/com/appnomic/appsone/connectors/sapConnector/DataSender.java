package com.appnomic.appsone.connectors.sapConnector;

import com.appnomic.appsone.common.protbuf.CollatedTransactionsProtos;
import com.appnomic.appsone.common.protbuf.KPIAgentMessageProtos;
import com.appnomic.appsone.common.protbuf.MessagePublisherGrpc;
import com.appnomic.appsone.common.protbuf.RPCServiceProtos;
import com.appnomic.appsone.connectors.utill.ConfigDataReader;
import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import com.appnomic.appsone.connectors.utill.JSONDataParser;
import com.google.gson.JsonArray;
import io.grpc.ManagedChannel;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyChannelBuilder;
import io.netty.handler.ssl.SslProvider;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.SSLException;
import java.io.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class DataSender {
    private static final Logger logger = Logger.getLogger(DataSender.class);
    TransactionDataBuilder transactionDataBuilder = new TransactionDataBuilder();
    Map<String, String> processedData = new HashMap<>();
    Map<String, String> finalData = null;
    String agentID = null;
    List<Map<String, String>> finalList = null;
    List<Integer> unKnownAndGoodCountList =  null;
    Map<String, List<Integer>> countMap = null;
    private int unknownCount;
    private int goodCount;
    static String endTimeStampForHeal = null;
    static String endTimeStampToFile = null;
    static String currentTimestamp = null;
    LocalDateTime endTimeStampParsedFromInput = null;


    public static Map<String,String> setNewPara(Map<String,String> para) {

        String date = null;
        Map<String,String> timeMap = new LinkedHashMap<>();
        String startDateTime = null;
        String endDateTime =null;
//        if (module.equalsIgnoreCase("SWNC_GET_STATRECS_FRAME")){
//            endDateTime = para.get("READ_END_DATE")+para.get("READ_END_TIME");
//            startDateTime = para.get("READ_START_DATE")+para.get("READ_START_TIME");
//        }else if (module.equalsIgnoreCase("CNV_MBT_READ_ST22_DUMPS")){
//            endDateTime = para.get("IV_TO_DATE")+para.get("IV_TO_TIME");
//            startDateTime = para.get("IV_FROM_DATE")+para.get("IV_FROM_TIME");
//        }else {
//            startDateTime = para.get("IV_TIMESTAMP_FROM");
//            endDateTime = para.get("IV_TIMESTAMP_TO");
//        }
        startDateTime = para.get("startDate")+para.get("startTime");
        endDateTime = para.get("endDate")+para.get("endTime");
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime endDate = LocalDateTime.parse(endDateTime,inputFormatter).plusMinutes(1);
        LocalDateTime startDate = LocalDateTime.parse(startDateTime,inputFormatter).plusMinutes(1);
        String sDate = startDate.format(dateFormatter);
        String eDate = endDate.format(dateFormatter);
        String sTime = startDate.format(timeFormatter);
        String eTime = endDate.format(timeFormatter);
        timeMap.put("startDate",sDate);
        timeMap.put("startTime",sTime);
        timeMap.put("endDate",eDate);
        timeMap.put("endTime",eTime);
//        if (module.equalsIgnoreCase("SWNC_GET_STATRECS_FRAME")){
//            date="READ_START_DATE="+sDate+",READ_START_TIME="+sTime+",READ_END_DATE="+eDate+",READ_END_TIME="+eTime;
//        }else if (module.equalsIgnoreCase("CNV_MBT_READ_ST22_DUMPS")){
//            date="IV_FROM_DATE="+sDate+",IV_FROM_TIME="+sTime+",IV_TO_DATE="+eDate+",IV_TO_TIME="+eTime;
//        }else {
//            date =  "IV_TIMESTAMP_FROM="+sDate+sTime +",IV_TIMESTAMP_TO="+eDate+eTime;
//        }
        return timeMap;

    }
    public  static Map<String,String> setToTimeZone(Map<String,String> para,String offset) throws java.text.ParseException {
        String startTime=null,startDate = null,endDate=null,endTime = null;
        Map<String,String> timeMap = new LinkedHashMap<>();
        DateTimeFormatter f = DateTimeFormatter.ofPattern( "yyyyMMddHHmmss").withZone(ZoneId.of(offset));
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");
//        String timeZone = offset;
        String enStamp = null;
        String stStamp = null;
//        if (module.equalsIgnoreCase("SWNC_GET_STATRECS_FRAME")){
//            enStamp = para.get("READ_END_DATE")+para.get("READ_END_TIME");
//            stStamp = para.get("READ_START_DATE")+para.get("READ_START_TIME");
//        }else if (module.equalsIgnoreCase("CNV_MBT_READ_ST22_DUMPS")){
//            enStamp = para.get("IV_TO_DATE")+para.get("IV_TO_TIME");
//            stStamp = para.get("IV_FROM_DATE")+para.get("IV_FROM_TIME");
//        }else {
//            enStamp = para.get("IV_TIMESTAMP_FROM");
//            stStamp = para.get("IV_TIMESTAMP_TO");
//        }
        stStamp = para.get("startDate")+para.get("startTime");
        enStamp = para.get("endDate")+para.get("endTime");
//        System.out.println("Starting Stamp: "+stStamp);
//        System.out.println("Ending Stamp: "+enStamp);// Specify locale to determine human language and cultural norms used in translating that input string.
        LocalDateTime ldt = LocalDateTime.parse(enStamp,f);
        LocalDateTime ldts = LocalDateTime.parse(stStamp,f);
        LocalDateTime startTimeWithZone = ldts.atZone(ZoneId.systemDefault())
                .withZoneSameInstant(ZoneId.of(offset))
                .toLocalDateTime();
        LocalDateTime endTimeWithZone = ldt.atZone(ZoneId.systemDefault())
                .withZoneSameInstant(ZoneId.of(offset))
                .toLocalDateTime();
        endDate = endTimeWithZone.toLocalDate().format(dateFormatter);
        endTime = endTimeWithZone.toLocalTime().format(timeFormatter);
        startDate = startTimeWithZone.toLocalDate().format(dateFormatter);
        startTime = startTimeWithZone.toLocalTime().format(timeFormatter);
        String finalDate = null;
//        if (module.equalsIgnoreCase("SWNC_GET_STATRECS_FRAME")){
//            finalDate="READ_START_DATE="+startDate+",READ_START_TIME="+startTime+",READ_END_DATE="+endDate+",READ_END_TIME="+endTime;
//        }else if (module.equalsIgnoreCase("CNV_MBT_READ_ST22_DUMPS")){
//            finalDate="IV_FROM_DATE="+startDate+",IV_FROM_TIME="+startTime+",IV_TO_DATE="+endDate+",IV_TO_TIME="+endTime;
//        }else {
//            finalDate =  "IV_TIMESTAMP_FROM="+startDate+startTime +",IV_TIMESTAMP_TO="+endDate+endTime;
//        }
        timeMap.put("startDate",startDate);
        timeMap.put("startTime",startTime);
        timeMap.put("endDate",endDate);
        timeMap.put("endTime",endTime);
        logger.info("Load time "+finalDate);

//        logger.info("Load time date with Zone : "+ finalDate);
        return timeMap;

    }
    public static Map<String,String> covertToUTC(Map<String,String> para,String offset) throws java.text.ParseException {
        String endTimestamp = null,startTimestamp=null;
        String gmtTimestamp = null ;
        Map<String,String> inputMap = new LinkedHashMap<>();
        endTimestamp = para.get("endDate")+" "+ para.get("endTime");
        startTimestamp = para.get("startDate")+" "+para.get("startTime");
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmmss");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");
        LocalDateTime endlocalDateTime = LocalDateTime.parse(endTimestamp,inputFormatter);
        LocalDateTime startlocalDateTime = LocalDateTime.parse(startTimestamp,inputFormatter);
        ZoneId zone = ZoneId.of(offset);
        ZoneOffset endzoneOffSet = zone.getRules().getOffset(endlocalDateTime);
        ZoneOffset startzoneOffSet = zone.getRules().getOffset(startlocalDateTime);
        Instant endinstant = endlocalDateTime.toInstant(endzoneOffSet);
        Instant startinstant = startlocalDateTime.toInstant(startzoneOffSet);
        LocalDateTime utcstartDateTime = LocalDateTime.ofInstant(startinstant, ZoneOffset.UTC);
        LocalDateTime utcendDateTime = LocalDateTime.ofInstant(endinstant, ZoneOffset.UTC);
        inputMap.put("endDate",utcendDateTime.toLocalDate().format(dateFormatter));
        inputMap.put("endTime",utcendDateTime.toLocalTime().format(timeFormatter));
        inputMap.put("startDate",utcstartDateTime.toLocalDate().format(dateFormatter));
        inputMap.put("startTime",utcstartDateTime.toLocalTime().format(timeFormatter));
        return inputMap;

    }

    public static String setInput(Map<String,String> timestamp,String input){
        String formatedInput =null;
        String[] stringToken = input.split(",");

        if (stringToken.length==4){
            String s1 = stringToken[0].replace("yyyymmdd",timestamp.get("startDate"));
            String s2 = stringToken[1].replace("HHmmss",timestamp.get("startTime"));
            String s3  =stringToken[2].replace("yyyymmdd",timestamp.get("endDate"));
            String s4 =stringToken[3].replace("HHmmss",timestamp.get("endTime"));
            formatedInput = s1+","+s2+","+ s3+","+ s4;
        }else {
            String s1 =stringToken[0].replace("yyyymmddHHmmss",timestamp.get("startDate")+timestamp.get("startTime"));
            String s2 = stringToken[1].replace("yyyymmddHHmmss",timestamp.get("endDate")+timestamp.get("endTime"));
            formatedInput = s1+","+s2;
        }
        return formatedInput;
    }

    public static Map<String,String> createDateMap(String para){
        Map<String,String> data = new HashMap<>();
        StringTokenizer myToken = new StringTokenizer(para, ",");
        while (myToken.hasMoreTokens()) {
            StringTokenizer token = new StringTokenizer(myToken.nextToken(), "=");
            while (token.hasMoreTokens()) {
                String startDateToken = token.nextToken();
                data.put(startDateToken,token.nextToken());
            }
        }
        return data;
    }

    public static Map<String,String> runTimeDate(String offset){
//        String timeZone = "GMT+04:30";
        Map<String,String> timeMap = new LinkedHashMap<>();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
        DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HHmmss");
        LocalDateTime endDateTime = LocalDateTime.now();
        LocalDateTime endDateTimeWithZone = endDateTime.atZone(ZoneId.systemDefault())
                .withZoneSameInstant(ZoneId.of(offset)).toLocalDateTime().withSecond(0);
//        LocalDateTime endDateTime = LocalDateTime.ofInstant(instant,ZoneId.of(offset)).withSecond(0);
        LocalDateTime startDateTimeWithZone = endDateTimeWithZone.minusMinutes(1);
        String endDate = dateFormat.format(endDateTimeWithZone.toLocalDate());
        String endTime = timeFormat.format(endDateTimeWithZone.toLocalTime());
        String startDate = dateFormat.format(startDateTimeWithZone.toLocalDate());
        String startTime = timeFormat.format(startDateTimeWithZone.toLocalTime());
        String  finalDate =null;
//        if (module.equalsIgnoreCase("SWNC_GET_STATRECS_FRAME")){
//           finalDate="READ_START_DATE="+startDate+",READ_START_TIME="+startTime+",READ_END_DATE="+endDate+",READ_END_TIME="+endTime;
//        }else if (module.equalsIgnoreCase("CNV_MBT_READ_ST22_DUMPS")){
//            finalDate="IV_FROM_DATE="+startDate+",IV_FROM_TIME="+startTime+",IV_TO_DATE="+endDate+",IV_TO_TIME="+endTime;
//        }else {
//            finalDate =  "IV_TIMESTAMP_FROM="+startDate+startTime +",IV_TIMESTAMP_TO="+endDate+endTime;
//        }
        timeMap.put("startDate",startDate);
        timeMap.put("startTime",startTime);
        timeMap.put("endDate",endDate);
        timeMap.put("endTime",endTime);
        logger.info("RUN time "+finalDate);
        return timeMap;

    }
    public static String timestamp(Map<String,String> para,String offset) throws java.text.ParseException {
        String endTimestamp = null;
        String gmtTimestamp = null ;
        endTimestamp = para.get("endDate")+" "+ para.get("endTime");
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmmss");
        LocalDateTime localDateTime = LocalDateTime.parse(endTimestamp,inputFormatter);
        endTimeStampToFile = localDateTime.toLocalDate().toString();
        currentTimestamp = localDateTime.toString();
        ZoneId zone = ZoneId.of(offset);
        ZoneOffset zoneOffSet = zone.getRules().getOffset(localDateTime);
        Instant instant = localDateTime.toInstant(zoneOffSet);
        DateTimeFormatter formatterForHeal = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
        gmtTimestamp = LocalDateTime.ofInstant(instant,ZoneId.of("GMT")).format(formatterForHeal);
//        gmtTimestamp=endTimeStampForHeal;
//        logger.info("This is GMT timestamp :"+endTimeStampForHeal);
        return gmtTimestamp;

    }



    public void segregateWorkloadData(List<ArrayList<String>> data, String instance, Map<String, String> trans,String timestamp) throws IOException, ParseException {
        HashMap<String, List<String>> dataMap = new HashMap<>();
        List<String> columnNme = trans.keySet().stream().collect(Collectors.toList());
        List<Map<String,String>> rowData = new ArrayList<>();
        Map<String,String> map = null;
        for (List<String> list:data) {
            map = new LinkedHashMap<>();
            for (int i = 0; i < list.size(); i++) {
                map.put(columnNme.get(i),list.get(i));
            }
            rowData.add(map);
        }
        DateTimeFormatter formatterForHeal = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
        LocalDateTime localDateTime = LocalDateTime.parse(timestamp,formatterForHeal);
        LocalDate date = localDateTime.toLocalDate();
//       logger.info("Row transaction Data "+rowData);
        try{
            // Create file
            File f = new File("log/transaction"+date+".log");
            if(!f.exists()){
                f.createNewFile();
            }else{
                logger.info("File already exists");
            }
            FileWriter fileWriter = new FileWriter(f, true);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append("GMT Timestamp "+localDateTime);

            bufferedWriter.newLine();
            for (int i = 0; i < rowData.size(); i++) {
                for (String name:trans.keySet()) {
                    if (trans.get(name).equalsIgnoreCase("BOTH")||trans.get(name).equalsIgnoreCase("FILE")){
                        String s = name +" = "+ rowData.get(i).get(name) +"  ";
                        bufferedWriter.append(s);
                    }

                }
                bufferedWriter.newLine();
            }
            bufferedWriter.newLine();
            bufferedWriter.append("/////////////////////////////////////////////////////////");
            //Close the output stream
            bufferedWriter.close();
        }catch (Exception e){//Catch exception if any
           logger.error("Error ",e);
        }

        for (int i = 0; i < data.size(); i++) {
            List<String> tempList = data.get(i);
            if (!tempList.get(0).isEmpty()){
                if (dataMap.containsKey(tempList.get(0))) {
                    dataMap.get(tempList.get(0)).add(tempList.get(1));
                } else {
                    List<String> list = new ArrayList<>();
                    list.add(tempList.get(1));
                    dataMap.put(tempList.get(0), list);
                }
            }

        }
        countMap = new LinkedHashMap<>();
        List<String> tempList = null;
        for (String key : dataMap.keySet()) {
            unKnownAndGoodCountList = new ArrayList<>();
            unknownCount = 0;
            goodCount = 0;
            tempList = dataMap.get(key);
            for (String value : tempList) {
                if (value.isEmpty() || value == null || value.equals("")) {
                    unknownCount++;
                } else {
                    goodCount++;
                }
            }
            unKnownAndGoodCountList.add(goodCount);
            unKnownAndGoodCountList.add(unknownCount);
            if (goodCount > 0) {
                countMap.put(key, unKnownAndGoodCountList);
            }
            tempList.removeIf(x -> (x == null || x.isEmpty() || x.equals("")));
            //System.out.println("tempList : " + tempList);
            float respTimeSum = 0;
            if (!tempList.isEmpty()) {
                for (int i = 0; i < tempList.size(); i++) {
                    respTimeSum += Float.parseFloat(tempList.get(i));
                }
            }
            float size = tempList.size();
            float respAvg = 0;
            if (size != 0)
                respAvg = respTimeSum / size;
            if ((dataMap.get(key).size() != 0)) {
                processedData.put(key, String.valueOf(respAvg));
            }
        }

        //System.out.println(dataMap);
//        System.out.println("tempList :"+tempList);
//        System.out.println(countMap);
//        System.out.println(processedData);
        finalList = new ArrayList<>();
        for (Map.Entry<String, List<Integer>> entry1 : countMap.entrySet()) {
            finalData = new LinkedHashMap<>();
           String key = entry1.getKey();
           // System.out.println("key :"+key);
            List<Integer> count = entry1.getValue();
          //  System.out.println("count :"+count.get(0));
            String respTime = processedData.get(key);
           // System.out.println("respTime :"+respTime);
            String transaction = "/"+key+ConfigInitializer.INSTANCE_MAPPING.get(instance).get("transRule");
            String txnIdentifier = transaction;
            finalData.put("Transaction ID",transaction);
            finalData.put("Transaction Identifier",txnIdentifier);
            finalData.put("Average Response Time", respTime);
            finalData.put("Good", String.valueOf(count.get(0)));
            finalData.put("Unknown", String.valueOf(count.get(1)));
            finalData.put("Total Count",String.valueOf(count.get(0)+count.get(1)));
            finalList.add(finalData);
        }
//        System.out.println("finalList :"+finalList);
//        System.out.println(finalList.size());
        logger.info("GMT timestamp : "+timestamp);

//        Map<String,JSONObject> discoveryTransactionData = null;
//        if (!finalList.isEmpty())
//            discoveryTransactionData = new JSONDataParser().getTransactionDetails(finalList,instance);

//        if (!discoveryTransactionData.isEmpty()){
//            JSONObject transactionJsonSendToFile  = null;
//
//            for (String transactionIdentifier : discoveryTransactionData.keySet()) {
//                transactionJsonSendToFile = new JSONObject();
//                transactionJsonSendToFile.put("transId",transactionIdentifier);
//                logger.info("This is transaction that has to be added manually ");
//                logger.info("This is json to be added to file : "+transactionJsonSendToFile);
//                logger.info("This is json to create the transaction to HEAL : "+discoveryTransactionData.get(transactionIdentifier));
//                SAPClient.discoveryList.add(transactionJsonSendToFile);
//            }
//        }
//        else {
//            logger.info("No new transcation to add to HEAL and File");
//        }
//
        List<Map<String,String>> finalListRemovedDiscoveyTransaction = new ArrayList<>();
//        for (Map<String,String> transactionMap : finalList) {
//            List<String> listOfDiscoveredTransactionIdentifier = discoveryTransactionData.keySet().stream().collect(Collectors.toList());
//            if (!listOfDiscoveredTransactionIdentifier.contains(transactionMap.get("Transaction Identifier"))){
//                finalListRemovedDiscoveyTransaction.add(transactionMap);
//            }else{
//                logger.info("this transaction is removed from final list as it is not discoverd yet" + transactionMap);
//            }
//        }

        finalListRemovedDiscoveyTransaction=finalList;


        if (finalListRemovedDiscoveyTransaction.isEmpty()){
           logger.info("No data to send to HEAL");
        }else {
            for(int i=0; i<finalListRemovedDiscoveyTransaction.size(); i++) {
                sendTransactionData(finalListRemovedDiscoveyTransaction.get(i),timestamp,ConfigInitializer.INSTANCE_MAPPING.get(instance).get("transAgent"),instance);
            }
        }
    }



    public static ManagedChannel createNettyChannel() {
        ManagedChannel channel=null;
        try {

            channel = NettyChannelBuilder.forAddress(ConfigInitializer.HOST_NAME, Integer.parseInt(ConfigInitializer.GRPC_PORT))
                    .sslContext(GrpcSslContexts.forClient().sslProvider(SslProvider.OPENSSL).build()).build();


        } catch (NumberFormatException e1) {
           logger.error("Error while creating Channel",e1);
        } catch (SSLException e1) {
            logger.error("SSL exception ",e1);
        } finally {
            return channel;
        }
    }



    public static String sendKPIRequest(KPIAgentMessageProtos.KPIAgentMessage kpiMessage){

        ManagedChannel channel= createNettyChannel();
        MessagePublisherGrpc.MessagePublisherBlockingStub blockingStub = null;
        if (channel==null){
            logger.info("Channel is null not able to send the data");
        }else {
            blockingStub = MessagePublisherGrpc.newBlockingStub(channel);
        }

        RPCServiceProtos.ResponseCode response = null;

        /* Publishing the protobuf to gRPC */
        try{
                response = blockingStub.publishKPIAgentMessage(kpiMessage);
        }
        catch (Exception ae){
            logger.error("Error sending the data to GRPC",ae);
            return null;
        }
        channel.shutdown();
//        System.out.println("Response Code :" + response.toString());
        return response.toString();

    }

    public static String sendTransactionRequest(CollatedTransactionsProtos.CollatedTransactions collatedTransaction){

        ManagedChannel channel= createNettyChannel();
        MessagePublisherGrpc.MessagePublisherBlockingStub blockingStub = null;

        if (channel!=null)
            blockingStub = MessagePublisherGrpc.newBlockingStub(channel);
        else
            logger.info("Channel is null ");
        RPCServiceProtos.ResponseCode response = null;

        /* Publishing the protobuf to gRPC */
        try{
            response = blockingStub.publishCollatedTransactionMessage(collatedTransaction);
        }
        catch (Exception ae){
            logger.error("Error while sending to GRPC",ae);
//            logger.error("Response Code : Null");
            return null;
        }
        channel.shutdown();
//        System.out.println("Response Code :" + response.toString());
        return response.toString();

    }

    public void sendTransactionData(Map<String, String> params, String endTimeStampForHeal, String agentID,String instance) {
        transactionDataBuilder.buildTransactionData(params, endTimeStampForHeal, agentID,instance);
    }
}