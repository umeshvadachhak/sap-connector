package com.appnomic.appsone.connectors.sapConnector;

import com.appnomic.appsone.connectors.utill.ConfigDataReader;
import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import com.sap.conn.jco.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.quartz.TriggerBuilder.newTrigger;

/**
 * SAP Client to execute pre-defined SAP functions.
 */
public class SAPClient implements Job{
    // This is passed as an argument Array [3] and value will look like
    // "ABAP_AS_WITH_POOL";

    static String ABAP_AS_POOLED = "";
    static JCoTable table;
    static JCoTable statsTable;
    static int rowCount = 0;
    static JCoStructure struct;
    static boolean isTable = false;
    static String structToken;
    static String mode;
    static String[] arguments;
    static String gmtOffset;
    private static ArrayList<ArrayList<String>> transactionList = null;
    static ArrayList<ArrayList<ArrayList<String>>> totalChunkData = null;
    static ArrayList<ArrayList<String>> filteredChunkData = null;
    static LinkedHashSet<String> colummFilterd = new LinkedHashSet<>();
    static Map<String,String> colummFilterdMap = new LinkedHashMap<>();
    private static final Logger log = Logger.getLogger(SAPClient.class);
    public static List<JSONObject> discoveryList = new ArrayList<>();
    public static String finalTime = null;
    public static String statrecInput = null;
    public static String dumpsInput = null;
    public static String errorInput = null;
    public static Map<String,String> inputMapping = new HashMap<>();
    public static Map<String,String> additionalKPIs= new HashMap<>();
//    private static final Logger log = Logger.getLogger(SAPClient.class);
    // Config file that has function + table to KPIs file mapping
    // Format : function=/path/to/table1name.cfg,/path/to/table2name.cfg,.......
    static String configFile = System.getProperty("sapConnector.config.file");

    /**
     * The following function executeSAPFunction executes a simple RFC function
     * STFC_CONNECTION.
     *
     * @param functionName  SAP RFC Enabled function that has to be executed
     * @param functionParam SAP RFC Enabled function's parameter. This would be in
     *                      the following format key1=value1, key2=value2,
     *                      key3=value3 (Where keyx = ABAP Function Param Name
     *                      valuex = ABAP Function Param Value)
     * @return JCoFunction The response function, fetch based on the input provided.
     * @throws JCoException
     */
    public static JCoFunction executeSAPFunction(String functionName, String functionParam) throws JCoException {

        // JCoDestination is the logic address of an ABAP system and ...
        JCoDestination destination = JCoDestinationManager.getDestination(ABAP_AS_POOLED);
        // ... it always has a reference to a metadata repository
        JCoFunction function = destination.getRepository().getFunction(functionName);
        if (function == null)
            throw new RuntimeException(functionName + " Function not found in SAP.");

        // JCoFunction is container for function values. Each function contains separate
        // containers for import, export, changing and table parameters.
        // To set or get the parameters use the APIS setValue() and getXXX().
        // Parameters can be in the form of key1=value1, key2=value2, key3=value3
        // each key value par has to be set separately.

        StringTokenizer st = new StringTokenizer(functionParam, ",");
        if (st.countTokens() == 0) {
            // This is a scenario where only 1 key value pair is given without delimiter.
            StringTokenizer keyvalue = new StringTokenizer(functionParam, "=");
            if (keyvalue.countTokens() != 2) {
                throw new RuntimeException(
                        "Wrong Parameter format. should be given in " + "key=value, key=value, ......");
            } else {
                function.getImportParameterList().setValue(keyvalue.nextToken(), keyvalue.nextToken());
            }
        } else {
            while (st.hasMoreTokens()) {
                StringTokenizer keyvalue = new StringTokenizer(st.nextToken(), "=");
                if (keyvalue.countTokens() != 2) {
                    // ASSUMPTION: Case where the input can have a table to be sent
                    // Still to code !!!!!!!

                    throw new RuntimeException(
                            "Wrong Parameter format. should be given in " + "key=value, key=value, ......");
                } else {
                    function.getImportParameterList().setValue(keyvalue.nextToken(), keyvalue.nextToken());
                }
            }

        }

        // execute, i.e. send the function to the ABAP system addressed
        // by the specified destination, which then returns the function result.
        // All necessary conversions between Java and ABAP data types
        // are done automatically.
        function.execute(destination);

        return function;
    }

    /*
     *
     *
     */
    public static JCoTable execTableFunction(String functionalMOduleName,String timestamp,String tableName) throws JCoException {
        isTable = false;
        JCoTable tempTble = null;
//        if (inputs.length != 6)
//            throw new RuntimeException("Wrong parameters: Array [0] = functionName Array [1] = functionParam"
//                    + " Array [2] = table name,Array[3] = SAP config file  Array[4] = GMT offset  Array[5] = Mode");
        ABAP_AS_POOLED = ConfigInitializer.DESTINATION_FILE;
        JCoFunction response = executeSAPFunction(functionalMOduleName, timestamp);
//        myDebug("Response Received: " + response.getName());
        log.info("Response Received "+response.getName());
        JCoParameterList tableparams = response.getTableParameterList();

        // There are 2 ways a table data can be exposed in ABAP Function
        // 1. Directly the function can create a tabled output and expose all the
        // fields of output to a table
        // Example query for which output is a direct table.
        // "ZRFC_MONITORING_DATA" "DATE=20181205" "OUTTAB"
        // 2. Create an export list and in the export list provide a table output
        // Example query for which output is a direct table.
        // "ZAPP_GET_WORKLOAD"
        // "READ_START_DATE=20181205,READ_START_TIME=105500,READ_END_DATE=20181205
        // ,READ_END_TIME=110000" "HITLIST_DATABASE"
        // hence if a response parameter list is not like a table then try fetching
        // the export parameter list of response and get the table from it.

        // The input[2] can be of the format TableName:SubTable:SubTable-SubTable:......
        // Example ALL_STATRECS:STATRECS:MAINREC
        // need to iterate based on the ":" as a token
        // if ":" not available then directly pass input[2] as a Table Name to fetch
        // Each structure record is embedded in a table field. Hence need to tavese the
        // table
        // not the structure

        // table = response.getTableParameterList().getTable(inputs[2]);

//        System.out.println("input 2 : "+recName);
//        StringTokenizer myToken = new StringTokenizer(recName, ":");
        StringTokenizer myToken = new StringTokenizer(tableName,":");
//       log.info("mytoken : "+myToken);
        if (myToken.countTokens() == 1)
            isTable = true;
        if (tableparams != null) {
            tempTble = response.getTableParameterList().getTable(myToken.nextToken());
        } else {
//            myDebug("Export List Extracted: ");
            tempTble = response.getExportParameterList().getTable(myToken.nextToken());
        }

            while (myToken.hasMoreTokens()) {
                String subToken = myToken.nextToken();
                myDebug(subToken + " ::: " + table.getValue(subToken).getClass().getName());
                if (table.getValue(subToken).getClass().getName()
                        .equalsIgnoreCase("com.sap.conn.jco.rt.DefaultStructure")) {
                    structToken = subToken;
                } else if (table.getValue(subToken).getClass().getName()
                        .equalsIgnoreCase("com.sap.conn.jco.rt.DefaultTable")) {
                    table = table.getTable(subToken);
                    if (myToken.countTokens() == 0)
                        isTable = true;
                } else {
                    // unknown structure as response or input is wrong
//                    myDebug("ERROR : Wrong Input or output structure / table unknown");
                    log.error("ERROR : Wrong Input or output structure / table unknown");
                    throw new RuntimeException("Wrong input parameters or output structure / table format not known");
                }
            }



//        // Debug data
//        if (System.getProperty("myDebug.config.value").equals("1")) {
//            if (table != null && isTable) {
////                myDebug("Table Extracted: ");
////                myDebug(table.toString());
////                System.out.println(table);
//            } else {
////                myDebug("Structure Extracted: ");
//                while (table.nextRow()) {
//                    struct = table.getStructure(structToken);
////                    System.out.println(struct);
//                    for (JCoField field : struct) {
////                        myDebug(field.getName() + " :::: " + field.getString());
//                    }
//                }
//            }
//            table.firstRow();
//        }
        //System.out.println("struct :"+struct);
        return tempTble;
    }
    /*
     * Prints based on the debug level set. For now implemented only 1 = print 0 =
     * no print
     */
    static void myDebug(String payload) {
//        if (System.getProperty("myDebug.config.value").equals("1"))
            System.out.println(new java.sql.Timestamp(System.currentTimeMillis()) + " ::: " + payload);
    }

    /*
     * The following method return if there are any more entries in the table
     * buffer. Assumption is execTableFuntion() is executed before checking hasMore.
     */
    public static boolean isLast() {
        return table.isLastRow();
    }

    /*
     * The following method fetches the JCOTable details in the lots of sizes
     * mentioned as a parameter.This helps in fetching large amounts of data in
     * chunks. The chunk size can be passed as a parameter setSize. The return value
     * will be null if there is nothing left to fetch.
     *
     *
     */

    public static List<ArrayList<String>> getTransData(String sapInstance,String args) throws FileNotFoundException, JCoException {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        for (int k = 0; k < table.getNumRows(); k++) {
            table.setRow(k);
            if (table.getString("INSTANCE").equalsIgnoreCase(sapInstance)){
                statsTable=table.getTable("STATRECS");
                String eId = null;
                String rTime = null;
                List<String> colNames = getFiltersForTableData(args, "TRANS").keySet().stream().collect(Collectors.toList());
//                System.out.println(colNames);
                for (int i = 0; i < statsTable.getNumRows(); i++) {
                    statsTable.setRow(i);
                    ArrayList<String> rowDta = new ArrayList<>();
                    JCoStructure mainTable = statsTable.getStructure("MAINREC");
                    for (int colCount = 0; colCount < colNames.size(); colCount++) {
                        String cname = colNames.get(colCount);
                        eId = mainTable.getString(cname).replaceAll("\\s.*","");
//                        if (eId.isEmpty()||eId==null)
//                            break;
                        rowDta.add(eId);
                    }
                    data.add(rowDta);
                }
            }
        }

        return data;
    }
    public static ArrayList<ArrayList<String>> getNextData(Map<String,List<String>> colTable,String taskType,Map<String,Map<String,String>> tableList,String sapInstance) throws FileNotFoundException, JCoException {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        List<String> tablesName = colTable.get(taskType);
//            List<String> colNames = getFiltersForTableData(funName,tableName);
            ArrayList<String> filteredCols = new ArrayList<String>();
            // Iterate throught the JCOTable to filter the data
        for (int k = 0; k < table.getNumRows(); k++) {
            table.setRow(k);
            String instanceName = table.getString("INSTANCE");
            if (instanceName.equalsIgnoreCase(sapInstance)){
                statsTable=table.getTable("STATRECS");
               log.info("This is No of Rows for Stats Table == "+statsTable.getNumRows());
                for (int i = 0; i < statsTable.getNumRows(); i++) {
                    statsTable.setRow(i);
                    ArrayList<String> rowDta = new ArrayList<>();
                    StringTokenizer tokenizer = new StringTokenizer(taskType,",");
                    while (tokenizer.hasMoreTokens()){
                        taskType = tokenizer.nextToken();
                        if (statsTable.getStructure("MAINREC").getString("TASKTYPE").equalsIgnoreCase(taskType)) {
                            for (String tableName : tablesName) {
                                List<String> colNames = tableList.get(tableName+".cfg").keySet().stream().collect(Collectors.toList());
                                colummFilterd.addAll(colNames);
                                colummFilterdMap.putAll(tableList.get(tableName+".cfg"));
                                for (int colCount = 0; colCount < colNames.size(); colCount++) {
                                    String cname = colNames.get(colCount);
                                    if (statsTable.getMetaData().isTable(tableName)) {
                                        // Use this cname as column name of the JCO table header and fetch value for a
                                        // row
                                        JCoTable temptable = statsTable.getTable(tableName);
                                        if (!temptable.isEmpty()){
                                            String temp = temptable.getString(cname.trim()).replaceAll("\\s.*", "");
//                                        if (temp.isEmpty()||temp==null)
//                                            break;
                                            rowDta.add(temp);
                                        }

//                        filteredCols.add(table.getString(cname));
                                    } else {
                                        // This is the case where table has a structure in it
                                        JCoStructure tempStuct = statsTable.getStructure(tableName);
//                        tempStuct.getString("TCODE");
                                        String temp = tempStuct.getString(cname).replaceAll("\\s.*", "");
//                                    if (temp.isEmpty()||temp==null)
//                                        break;
                                        rowDta.add(temp);

                                    }
                                }
                            }
                        }
                    }

                    if (!rowDta.isEmpty()) data.add(rowDta);
                }
            }else {
                log.info("This instance Data is not available");
            }

        }

        return data;
        }



    /*
     * The current function gets the filters defined in the table and function file.
     * These are the column names of the table that is needed. Parameters passed are
     * the function name and the table name in the response. Return value is an
     * array of the column names.
     *
     * @TODO: Aggregation based on column name.
     */


    public static Map<String,String> getFiltersForTableData(String funcName, String tabName)
            throws JCoException, FileNotFoundException {
        ArrayList<String> colFilters = new ArrayList<String>();
        Map<String,String> colMapFilter = new LinkedHashMap<>();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(configFile));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe.toString());
        }
        // tabfiles is of format tabnam1.file,tabname2.file,tabname3.file,......
        // tokenize to fetch each tabname and equate with tabName parameter
        String tabfiles = prop.getProperty(funcName);
        StringTokenizer stk = new StringTokenizer(tabfiles, ",");
        String filename = "";
        while (stk.hasMoreTokens()) {
            String tabfname = stk.nextToken();
            if (tabfname.endsWith(tabName + ".cfg")) {
                filename = System.getProperty("user.dir") + System.getProperty("file.separator")+"/config/" + tabfname;
                break;
            }
        }
        if (!filename.equals("")) {
            Scanner filescanner = new Scanner(new File(filename));
            while (filescanner.hasNext()) {
                String temp = filescanner.nextLine();
                colFilters.add(temp);
                String[] a = temp.split(" : ");
                if (a.length!=0)
                    colMapFilter.put(a[0],a[1]);
            }
            filescanner.close();
        }
        return colMapFilter;
    }

    public static Map<String,Map<String,String>> getColumnList(String funcName)
            throws JCoException, FileNotFoundException {
        ArrayList<String> colFilters = new ArrayList<String>();
        Map<String,Map<String,String>> tableColumnMap = new HashMap<>();
        Map<String,String> colMapFilter = null;

        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(configFile));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe.toString());
        }
        // tabfiles is of format tabnam1.file,tabname2.file,tabname3.file,......
        // tokenize to fetch each tabname and equate with tabName parameter
        String tabfiles = prop.getProperty(funcName);
        StringTokenizer stk = new StringTokenizer(tabfiles, ",");
        String filename = "";
        while (stk.hasMoreTokens()) {
            String tabfname = stk.nextToken();
            String tableName = "";
            StringTokenizer tkn = new StringTokenizer(tabfname,":");
            while (tkn.hasMoreTokens()){
                String temp = tkn.nextToken();
                if (temp.contains(".cfg"))
                    tableName= temp;
            }
            filename = System.getProperty("user.dir") + System.getProperty("file.separator")+"/config/" + tabfname;
            colMapFilter = new LinkedHashMap<>();
            Scanner filescanner = new Scanner(new File(filename));
            while (filescanner.hasNext()) {
                String temp = filescanner.nextLine();
                colFilters.add(temp);
                String[] a = temp.split(" : ");
                if (a.length!=0)
                    colMapFilter.put(a[0],a[1]);

            }
            tableColumnMap.put(tableName,colMapFilter);
            filescanner.close();
            }

        return tableColumnMap;
    }


    public static void main(String[] args) throws JCoException, IOException, InterruptedException, java.text.ParseException {
        //String[] configParams ={"SWNC_GET_STATRECS_FRAME","READ_START_DATE=20200420,READ_START_TIME=105000,READ_END_DATE=20200420,READ_END_TIME=105100","ALL_STATRECS:STATRECS:MAINREC","ECC_ABAP_AS_WITH_POOL"};
        arguments=args;
        ConfigInitializer.parseConfigFile("config/config.json");

        String log4jConfPath = ConfigInitializer.LOG_FILE;
        PropertyConfigurator.configure(log4jConfPath);
        log.info(System.currentTimeMillis());
//        System.setProperty("chunk.size.value","20");
//        System.setProperty("myDebug.config.value","1");
//        System.setProperty("sapConnector.config.file","./sapConnector.cfg");
//        KPIDataSender kpiDataSender = new KPIDataSender();
//        DataSender dataSender = new DataSender();
//        System.out.println(System.getProperty("user.dir"));
//        System.out.println("config :"+configFile);

        if (args.length == 0) {
        } else {
            log.info("Arguments: "+args.length);
//            System.out.println("Arguments: " + args.length);
        }

        ConfigDataReader configDataReader = new ConfigDataReader();
        configDataReader.sendRequest();
        mode = ConfigInitializer.MODE;
        gmtOffset = ConfigInitializer.TIME_ZONE;
        List<Map<String, String>> functionalList = ConfigInitializer.FUNCTIONAL_MODULES;
        System.out.println(ConfigInitializer.TIMESTAMP_MAPPING);
        Map<String,String> timestamMapping = ConfigInitializer.TIMESTAMP_MAPPING;
//        System.out.println(functionalList);
//        for (int i = 0; i < functionalList.size(); i++) {
//            String module = functionalList.get(i).get("module");
            if (mode.equalsIgnoreCase("RUN")){
                log.info("Running in Run MOde");
                inputMapping = DataSender.runTimeDate(gmtOffset);
//                inputMapping.put(module,temp);
            }else {
                log.info("Running in LOAD mode");
//                Map<String, String> dateMap = DataSender.createDateMap(input);
                inputMapping = DataSender.setToTimeZone(timestamMapping,gmtOffset);
//                inputMapping.put(module,temp);
            }
//        }
//        System.out.println(ConfigInitializer.TIMESTAMP_MAPPING);
//        System.out.println(inputMapping);
//        discoveryList = getDiscoveryJSONFile();
//        log.info("Discovry transaction List : "+discoveryList);
//        System.out.println(ConfigInitializer.FUNCTIONAL_MODULES);
//        System.out.println("gmt  offset"+DataSender.covertToUTC(inputMapping,gmtOffset));
//        System.out.println(inputMapping);

        try {
            log.info("Scheduler is starting");
            //             date job details..
            JobDetail jobDetail = JobBuilder.newJob(SAPClient.class).withIdentity("StartSAPConnector"+System.currentTimeMillis()).build();
            long millis=System.currentTimeMillis();
            java.util.Date date=new java.util.Date(millis);
            // specifying job trigger that run every 60 sec
//            SimpleTrigger triggers = (SimpleTrigger) newTrigger().withIdentity().

            Trigger trigger = newTrigger()
                    .startAt(date)
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(ConfigInitializer.FETCH_INTEVAL).repeatForever())
                    .build();
            // Run every 5 seconds.
            log.info("job details: "+ jobDetail.getKey().getName());
            // Schedule the job
            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            Scheduler scheduler = schedulerFactory.getScheduler();
            scheduler.scheduleJob(jobDetail, trigger);
            scheduler.start();

            log.info("trigger info "+trigger.getDescription());


        } catch (SchedulerException e) {
           log.error("This is error in schduler "+e.getStackTrace());
        }

    }



    private static Map<String, List<String>> getJSONFile() {
        Map<String,List<String>> mapForFilter = null;
        JSONParser jsonParser = new JSONParser();
        JSONObject json = null;
        List<String> taskValue = null;
        try {
            FileReader fr = new FileReader(ConfigInitializer.TASK_FILE);
//            log.info("file strea :" + fr.toString());
            json = (JSONObject) jsonParser.parse(fr);
//            System.out.println("JSON DATA :" + json);
            Iterator iterator = json.entrySet().iterator();
            mapForFilter = new HashMap<>();
            while (iterator.hasNext()) {
                taskValue = new ArrayList<>();
                Map.Entry<String, JSONArray> entry = (Map.Entry<String, JSONArray>) iterator.next();
                JSONArray value  = entry.getValue();
//                System.out.println("Value :"+value);
                for(int i=0; i<value.size();i++){
//                    System.out.println(value.get(i));
                    taskValue.add((String)value.get(i));
                }
                mapForFilter.put(entry.getKey(),taskValue);
            }
//            System.out.println("Map :"+mapForFilter);
            fr.close();
        } catch (FileNotFoundException e) {
            log.error("Getting file not found exception ",e);
        } catch (IOException e) {
           log.error("Io exception",e);
        } catch (ParseException e) {
            log.error("Parse Exception",e);
        }
        return mapForFilter;
    }


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        KPIDataSender kpiDataSender = new KPIDataSender();
        DataSender dataSender = new DataSender();
        String gmtTimestamp = null;
        log.info("This is the arguments");
        for (String s : arguments) {
            log.info(s);
        }
        String module = null;
        List<Map<String, String>> functionalList = ConfigInitializer.FUNCTIONAL_MODULES;
        for (int i = 0; i < functionalList.size(); i++) {
            String functionlMOdule =functionalList.get(i).get("module");
//            String inputs = inputMapping.get(functionlMOdule);
//            System.out.println(inputMapping );
//            System.out.println(functionalList.get(i).get("input"));
            String  input = null;
            if (functionlMOdule.equalsIgnoreCase("/SDF/GPC_GET_TRFC_ERRORS")){
                Map<String,String> map = null;
                try {
                    map = DataSender.covertToUTC(inputMapping,gmtOffset);
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                input = DataSender.setInput(map,functionalList.get(i).get("input"));
            }else {
                input = DataSender.setInput(inputMapping,functionalList.get(i).get("input"));
            }
            log.info("input "+input);
            String tableName = functionalList.get(i).get("table");
            JCoTable tempTable = null;
            try {
                tempTable = execTableFunction(functionlMOdule,input,tableName);
                log.info("Table :"+tempTable);
            } catch (JCoException e) {
                log.error("Error While Fetching the table ", e);
            }
            if (functionalList.get(i).get("module").equalsIgnoreCase("SWNC_GET_STATRECS_FRAME")){
                table=tempTable;
                module = functionalList.get(i).get("module");
                finalTime = input;
            }else if (functionalList.get(i).get("module").equalsIgnoreCase("CNV_MBT_READ_ST22_DUMPS")){
                additionalKPIs.put("ABAP DUMP Count",String.valueOf(tempTable.getNumRows()));
            }else {
                additionalKPIs.put("TRFC ERRORS Count",String.valueOf(tempTable.getNumRows()));
            }
        }
        try {
            gmtTimestamp = DataSender.timestamp(inputMapping, gmtOffset);
            log.info("This is GMT timestamp "+gmtTimestamp);
        } catch (java.text.ParseException e) {
            log.error("ERROR While converting the time to GMT", e);
        }
        log.info("SETTING THE NEW PARAMETER ");
        log.info("old parameters " + inputMapping);
        Map<String,String> tempInputMap = new HashMap<>();
//        for (String s: inputMapping.keySet()) {
//            String tempInput = inputMapping.get(s);
//            Map<String,String> tempDate = DataSender.createDateMap(tempInput);
//            String newParameter = DataSender.setNewPara(tempDate,s);
//            tempInputMap.put(s,newParameter);
//        }
        inputMapping = DataSender.setNewPara(inputMapping);
        log.info("new parameters " + inputMapping);
        System.out.println(additionalKPIs);
        Map<String, Map<String, String>> fileMapping = null;
        Map<String, Map<String, String>> instanceMapping = ConfigInitializer.INSTANCE_MAPPING;
        try {
            fileMapping = getColumnList(module);
        } catch (JCoException e) {
            log.error("Jco exception", e);
        } catch (FileNotFoundException e) {
            log.error("File not Fount Exception", e);
        }
        for (String sapInstance : instanceMapping.keySet()) {
            ArrayList<ArrayList<String>> testDataToSend = null;
            List<ArrayList<String>> tranData = null;
            for (String dataType : getJSONFile().keySet()) {
                try {
                    if (table.isEmpty() || table == null) {
                        log.info("Table is empty or Not able to retrive the table");
                    } else
                        testDataToSend = getNextData(getJSONFile(), dataType, fileMapping, sapInstance);
//                System.out.println(testDataToSend);
                } catch (FileNotFoundException e) {
                    log.error("File not found Exception ", e);
                } catch (JCoException e) {
                    log.error("Jco exception ", e);
                }
                try {
                    tranData = getTransData(sapInstance,module);
                } catch (FileNotFoundException e) {
                    log.error("FILE NOT FOUNT EXCEPTION", e);
                } catch (JCoException e) {
                    log.error("JCO exception", e);
                }
                log.info("This is transaction data += " + tranData);
//                Map<String,String> dateMap = DataSender.createDateMap(arguments[1]);
//                ArrayList<String> columList = (ArrayList<String>) colummFilterd.stream().collect(Collectors.toList());
                if (testDataToSend.size() != 0) {
                    KPIDataSender.kpiDataKeepInMap(testDataToSend, colummFilterdMap, dataType, sapInstance, gmtTimestamp);
                } else {
                    log.info("There is no " + dataType + " data is availale for this timestamp");
                }
            }

            if (!tranData.isEmpty()) {
                try {
                    dataSender.segregateWorkloadData(tranData,sapInstance,getFiltersForTableData(module,"TRANS"),gmtTimestamp);
                } catch (IOException | JCoException | ParseException e) {
                    log.error("Error",e);
                }
            }else {
                log.info("There is no Transaction data available for this timestamp");
            }
        }
            log.info(System.currentTimeMillis());
        }

}
