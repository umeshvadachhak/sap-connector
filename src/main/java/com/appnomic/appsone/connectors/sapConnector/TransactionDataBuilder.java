package com.appnomic.appsone.connectors.sapConnector;
import com.appnomic.appsone.common.protbuf.CollatedTransactionsProtos;
import com.appnomic.appsone.common.protbuf.PSAgentMessageProtos;
import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import org.apache.log4j.Logger;

import java.util.Map;

public class TransactionDataBuilder {

    Logger log = Logger.getLogger(TransactionDataBuilder.class);
    public String buildTransactionDatanew(Map<String,String> params, String startTime,String agentID) {


        String transactionID= params.get("Transaction ID");

        CollatedTransactionsProtos.CollatedTransactions.Builder collatedTransactionBuilder = CollatedTransactionsProtos.CollatedTransactions.newBuilder()
                .setAgentUid(agentID) // extracting and setting the agent Uid
                .setStartTimeInGMT(startTime)    // extracting and setting the start Time
                .setRuleId(ConfigInitializer.RULE_ID)
                .setTransactionId(transactionID)
                .setEnterpriseName(ConfigInitializer.ENTERPRISENAME)
                .setTimezoneOffsetInSeconds(0);  // extracting and setting transactionId

        float averageResponseTimeInMicroseconds=Float.parseFloat(params.get("Average Response Time"));
        PSAgentMessageProtos.ResponseTime.ResponseTimeType responseTimeType= PSAgentMessageProtos.ResponseTime.ResponseTimeType.valueOf("DC");
        int totalCount=Integer.parseInt(params.get("Total Count"));

//        if(ConfigInitializer.TRANSACTION_RESPONSE_TIME.equalsIgnoreCase("MilliSeconds"))
            averageResponseTimeInMicroseconds=averageResponseTimeInMicroseconds*1000;

        System.out.println(averageResponseTimeInMicroseconds);
        CollatedTransactionsProtos.Response.Builder responseBuilder = CollatedTransactionsProtos.Response.newBuilder()
                .setResponseTimeType(responseTimeType)
                .setAvgResponseTimeInMicroseconds(averageResponseTimeInMicroseconds)
                .setTotalCount(totalCount);

        if(params.get("Good")!=null){
            int goodTransactionCount=Integer.parseInt(params.get("Good"));
            if (goodTransactionCount!=0){
                responseBuilder.addCounts(CollatedTransactionsProtos.Response.CollatedCount.newBuilder().
                        setResponseStatusTag(PSAgentMessageProtos.ResponseTime.ResponseStatusTag.GOOD).
                        setCount(goodTransactionCount).build());
            }
        }
        if(params.get("Fail")!=null){
            int failTransactionCount=Integer.parseInt(params.get("Fail"));
            if (failTransactionCount!=0){
                responseBuilder.addCounts(CollatedTransactionsProtos.Response.CollatedCount.newBuilder().
                        setResponseStatusTag(PSAgentMessageProtos.ResponseTime.ResponseStatusTag.FAIL).
                        setCount(failTransactionCount).build());
            }
        }
        if(params.get("Slow")!=null){
            int slowTransactionCount=Integer.parseInt(params.get("Slow"));
            if (slowTransactionCount!=0){
                responseBuilder.addCounts(CollatedTransactionsProtos.Response.CollatedCount.newBuilder().
                        setResponseStatusTag(PSAgentMessageProtos.ResponseTime.ResponseStatusTag.SLOW).
                        setCount(slowTransactionCount).build());
            }
        }
        if(params.get("Timeout")!=null){
            int timeoutTransactionsCount=Integer.parseInt(params.get("Timeout"));
            if (timeoutTransactionsCount!=0){
                responseBuilder.addCounts(CollatedTransactionsProtos.Response.CollatedCount.newBuilder().
                        setResponseStatusTag(PSAgentMessageProtos.ResponseTime.ResponseStatusTag.TIMEOUT).
                        setCount(timeoutTransactionsCount).build());
            }
        }
        if(params.get("Unknown")!=null){
            int unknownTransactionCount=Integer.parseInt(params.get("Unknown"));
            if (unknownTransactionCount!=0){
                responseBuilder.addCounts(CollatedTransactionsProtos.Response.CollatedCount.newBuilder().
                        setResponseStatusTag(PSAgentMessageProtos.ResponseTime.ResponseStatusTag.UNKNOWN).
                        setCount(unknownTransactionCount).build());
            }
        }

        // Building and adding response to the Builder
        CollatedTransactionsProtos.Response response = responseBuilder.build();
        collatedTransactionBuilder.addResponse(response);

        //collatedTransactionBuilder.putAllBizResponseCodes(null);

        CollatedTransactionsProtos.CollatedTransactions collatedTransaction = collatedTransactionBuilder.build();
        log.info("Sending collated record for the last minute "+startTime+ " to gRPC");

        // Pushing the collated message to the queue
        String responseFromGRPC = DataSender.sendTransactionRequest(collatedTransaction);
//        String responseFromGRPC = "testing ";
        return responseFromGRPC;

    }



    public void buildTransactionData(Map<String, String> params, String endTimeStampForHeal, String agentId,String instance) {
        String transactionID= params.get("Transaction Identifier");
        int goodTransactionCount = 0;
        int unknownTransactionCount = 0;
//        String transactionID = "POST#"+params.get("Transaction ID")+"|srv=fd3a546b-4a37-4687-bdf7-2199d07c9ad6|acc=5";

        CollatedTransactionsProtos.CollatedTransactions.Builder collatedTransactionBuilder = CollatedTransactionsProtos.CollatedTransactions.newBuilder()
                .setAgentUid(agentId)
                .setStartTimeInGMT(endTimeStampForHeal)
                .setTransactionId(transactionID);


        float averageResponseTimeInMicroseconds=Float.parseFloat(params.get("Average Response Time"));
        CollatedTransactionsProtos.Response.Builder responseBuilder = CollatedTransactionsProtos.Response.newBuilder()
                .setAvgResponseTimeInMicroseconds(averageResponseTimeInMicroseconds);
        responseBuilder.setResponseTimeType(PSAgentMessageProtos.ResponseTime.ResponseTimeType.DC);

        if(params.get("Good")!=null){
             goodTransactionCount=Integer.parseInt(params.get("Good"));
            if (goodTransactionCount!=0){
                responseBuilder.addCounts(CollatedTransactionsProtos.Response.CollatedCount.newBuilder().
                        setResponseStatusTag(PSAgentMessageProtos.ResponseTime.ResponseStatusTag.GOOD).
                        setCount(goodTransactionCount).build());
            }
        }

        if(params.get("Unknown")!=null){
             unknownTransactionCount=Integer.parseInt(params.get("Unknown"));
            if (unknownTransactionCount!=0){
                responseBuilder.addCounts(CollatedTransactionsProtos.Response.CollatedCount.newBuilder().
                        setResponseStatusTag(PSAgentMessageProtos.ResponseTime.ResponseStatusTag.UNKNOWN).
                        setCount(unknownTransactionCount).build());
            }
        }

        responseBuilder.setTotalCount(goodTransactionCount+unknownTransactionCount);
        // Building and adding response to the Builder
        CollatedTransactionsProtos.Response response = responseBuilder.build();
        collatedTransactionBuilder.addResponse(response);

        collatedTransactionBuilder.setRuleId(Integer.parseInt(ConfigInitializer.INSTANCE_MAPPING.get(instance).get("ruleId")));


        //collatedTransactionBuilder.putAllBizResponseCodes(null);

        CollatedTransactionsProtos.CollatedTransactions collatedTransaction = collatedTransactionBuilder.build();
//        System.out.println(collatedTransaction);
//        System.out.println("Sending collated record for the last minute "+endTimeStampForHeal+ " to queue");
//        System.out.println(collatedTransaction);

        // Pushing the collated message to the queue
        log.info("sening this transaction to GRPC");
        log.info(collatedTransaction);
        String responseFromGRPC = DataSender.sendTransactionRequest(collatedTransaction);
        log.info(responseFromGRPC);
//        System.out.println(responseFromGRPC);
//        QueuePublisher.sendCollatedMessage(collatedTransaction);
    }
}
