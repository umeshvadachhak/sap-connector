package com.appnomic.appsone.connectors.sapConnector;

import com.appnomic.appsone.common.protbuf.KPIAgentMessageProtos;
import com.appnomic.appsone.common.protbuf.MessagePublisherGrpc;
import com.appnomic.appsone.common.protbuf.RPCServiceProtos;
import com.appnomic.appsone.connectors.utill.ConfigDSJsonParser;
import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import io.grpc.ManagedChannel;
import org.apache.log4j.Logger;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;

import static com.appnomic.appsone.connectors.sapConnector.DataSender.*;

public class KPIDataSender {
    private static final Logger log = Logger.getLogger(KPIDataSender.class);
    private static List<Map<String,String>> tempList = null;
    private static Map<String,String> testMap = null;
    PrepareKPIData prepareKPIData;
    KPIAgentMessageProtos.KPIAgentMessage.Builder kpiMessageBuilder;
    String agentId;
    DataSender dataSender;
//    static float sum = 0;
//    static float avg = 0;
    public static void kpiDataKeepInMap(ArrayList<ArrayList<String>> kpiData,Map<String,String> columnMap,String taskType,String instance,String timestamp) {
//        System.out.println("kpiDataMainrec : "+kpiData);
        List<String> columnName = columnMap.keySet().stream().collect(Collectors.toList());
        Map<String,String> filterDMap = new LinkedHashMap<>();
        List<String> filteredColName = new ArrayList<>();
        tempList = new ArrayList<>();
        for (int i = 0; i < kpiData.size(); i++) {
            testMap = new LinkedHashMap<>();
            //System.out.println("kpi data : "+kpiDataMainRec.get(i));
            for (int j = 0; j < kpiData.get(i).size(); j++) {
                switch (taskType){
                    case "01":
                        testMap.put(columnName.get(j), kpiData.get(i).get(j));
                        break;
                    case "04":
                        testMap.put("BJ_"+columnName.get(j),kpiData.get(i).get(j));
                        break;
                    case "65":
                        testMap.put("HTTP_"+columnName.get(j),kpiData.get(i).get(j));
                        break;
                    case "FE":
                        testMap.put("RFC_"+columnName.get(j),kpiData.get(i).get(j));
                        break;
                }

            }
            //System.out.println("test map : "+testMap);
//            System.out.println(columnName);
            tempList.add(testMap);
        }

        List<String> paths = null;
        Map<String,String> pathMap = new LinkedHashMap<>();

        switch (taskType){
            case "01":
                filteredColName.addAll(columnName);
                filterDMap.putAll(columnMap);
                kpiDataFromMainRec(tempList, filterDMap,taskType,instance,timestamp);
                break;
            case "04":
//                paths = columnName.stream().map(c -> "BJ_" + c).collect(Collectors.toList());
                for (String key : columnMap.keySet()) {
                    pathMap.put("BJ_"+key,columnMap.get(key));

                }
//                filteredColName.addAll(paths);
                filterDMap.putAll(pathMap);
                kpiDataFromMainRec(tempList,filterDMap,taskType,instance,timestamp);
                break;
            case "65":
//                paths = columnName.stream().map(c -> "HTTP_" + c).collect(Collectors.toList());
                for (String key : columnMap.keySet()) {
                    pathMap.put("HTTP_"+key,columnMap.get(key));

                }
//                filteredColName.addAll(paths);
                filterDMap.putAll(pathMap);
                kpiDataFromMainRec(tempList, filterDMap,taskType,instance,timestamp);
                break;
            case "FE":
//                paths = columnName.stream().map(c -> "RFC_" + c).collect(Collectors.toList());
                for (String key : columnMap.keySet()) {
                    pathMap.put("RFC_"+key,columnMap.get(key));

                }
                filterDMap.putAll(pathMap);
//                filteredColName.addAll(paths);
                kpiDataFromMainRec(tempList, filterDMap,taskType,instance,timestamp);
                break;

        }


    }

    public static void kpiDataFromMainRec(List<Map<String,String>> kpiDataMainRec,Map<String,String> columnMap,String taskType,String instance,String timestamp) {
        List<Map<String, String>> segregatedList = new ArrayList<>();
//        System.out.println("KPi main list "+ kpiDataMainRec);
//        System.out.println("Filtered column "+columnMap);
        DateTimeFormatter formatterForHeal = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
        LocalDateTime localDateTime = LocalDateTime.parse(timestamp,formatterForHeal);
        LocalDate date = localDateTime.toLocalDate();

        try{
            // Create file
            File f = new File("log/kpi"+date+".log");
            if(!f.exists()){
                f.createNewFile();
            }else{
               log.info("File already exists");
            }
            FileWriter fileWriter = new FileWriter(f, true);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append("GMT Timestamp "+timestamp);

            bufferedWriter.newLine();
            for (int i = 0; i < kpiDataMainRec.size(); i++) {
                for (String name:columnMap.keySet()) {
                    if (columnMap.get(name).equalsIgnoreCase("BOTH")||columnMap.get(name).equalsIgnoreCase("FILE")){
                        String s = name +" = "+ kpiDataMainRec.get(i).get(name) +"  ";
                        bufferedWriter.append(s);
                    }

                }
                bufferedWriter.newLine();
            }
            bufferedWriter.newLine();
            bufferedWriter.append("/////////////////////////////////////////////////////////");
            //Close the output stream
            bufferedWriter.close();
        }catch (Exception e){//Catch exception if any
            log.error("Error: " ,e);
        }
        List<String> filteredColName = new ArrayList<>();
        for (int i = 0; i < kpiDataMainRec.size(); i++) {
            for (String name:columnMap.keySet()) {
               if (columnMap.get(name).equalsIgnoreCase("FILE"))
                   kpiDataMainRec.get(i).remove(name);
            }
            }

        for (String name : columnMap.keySet()) {
            if (!(columnMap.get(name).equalsIgnoreCase("FILE")))
                filteredColName.add(name);
        }
//        System.out.println("FInal listt column "+filteredColName);
//        System.out.println("FInal data list "+kpiDataMainRec);
        aggregatedRecsData(kpiDataMainRec,filteredColName,taskType,instance,timestamp);

    }
    public static void aggregatedRecsData(List<Map<String,String>> segregatedList,List<String> filteredColName,String taskType,String instnce,String timestamp) {
        Map<String,List<String>> tempFinalMap = null;
        Map<String,String> kpiListWithFix = new LinkedHashMap<>();
        Map<String,String> tempInnerMap = null;
        List<List<String>> columnList = new ArrayList<>();
        for(int j=0; j<filteredColName.size(); j++) {
            columnList.add(new ArrayList<String>());
        }
        for(int i=0; i<segregatedList.size(); i++) {
            tempFinalMap = new LinkedHashMap<>();
            tempInnerMap = segregatedList.get(i);
            for(int j=0; j<filteredColName.size(); j++) {
                if (tempInnerMap.containsKey(filteredColName.get(j))) {
                    columnList.get(j).add(tempInnerMap.get(filteredColName.get(j)));
                }
            }
        }
        for(int j=0; j<filteredColName.size(); j++) {
            tempFinalMap.put(filteredColName.get(j),columnList.get(j));
        }

        Map<String,String> kpiDataMap = new LinkedHashMap<>();
        List<String> kpiList = null;
        Set<Map.Entry<String,List<String>>> entrySet = tempFinalMap.entrySet();
        Iterator<Map.Entry<String,List<String>>> mapItr = entrySet.iterator();
        while(mapItr.hasNext()) {
            float sum = 0;
            float avg = 0;
            Map.Entry<String,List<String>> e = mapItr.next();
            kpiList = e.getValue();
//            System.out.println("For KPi == "+e);
            for(int i=0; i<kpiList.size(); i++) {
                sum = sum+Float.parseFloat(kpiList.get(i));
            }
//            System.out.println("sum "+sum);
//            System.out.println("size "+kpiList.size());
            if (!(kpiList.size()==0))
                avg = sum/kpiList.size();
            else
                avg = 0;
            //System.out.println("avg : "+avg);
            kpiDataMap.put(e.getKey(),String.valueOf(avg));
        }
        if (taskType.equalsIgnoreCase("01")){
            float data = Float.parseFloat(kpiDataMap.get("DSQLCNT"))+ Float.parseFloat(kpiDataMap.get("QUECNT")) +
                    Float.parseFloat(kpiDataMap.get("RFCCNT")) + Float.parseFloat(kpiDataMap.get("SLI_CNT"));
            kpiDataMap.put("FRONTEND",String.valueOf(data));

        }
        log.info("This is kpi List for Tasktype "+taskType+" and Instance "+instnce);
        log.info("Kpi data map : "+kpiDataMap);
        String kpiPrefix = ConfigInitializer.INSTANCE_MAPPING.get(instnce).get("kpiPrefix");
        String kpiPostfix = ConfigInitializer.INSTANCE_MAPPING.get(instnce).get("kpiPostfix");
        if (!kpiPrefix.equalsIgnoreCase("")){
            for (String s:kpiDataMap.keySet()) {
                kpiListWithFix.put(kpiPrefix+s,kpiDataMap.get(s));
            }
        }
        if (!kpiPostfix.equalsIgnoreCase("")){
            for (String s:kpiDataMap.keySet()) {
                kpiListWithFix.put(s+kpiPostfix,kpiDataMap.get(s));
            }
        }
//        System.out.println("Kpi list with fix = "+kpiListWithFix);
        if (!kpiListWithFix.isEmpty())
            kpiDataMap=kpiListWithFix;

        if (taskType.equalsIgnoreCase("04")){
            kpiDataMap.putAll(SAPClient.additionalKPIs);
        }
        log.info("kpi data with postfix and prefix "+kpiDataMap);
        Map<String, Object> configDsData = new ConfigDSJsonParser().getKpiDetails(ConfigInitializer.ACCOUNTID);
//        System.out.println(configDsData);
        System.out.println(configDsData);
        KPIDataSender dataSender = new KPIDataSender();
        System.out.println(configDsData);
        String instanceName = ConfigInitializer.INSTANCE_MAPPING.get(instnce).get("IntanceName");

        String response = dataSender.sendKpiData(configDsData,kpiDataMap,instanceName,timestamp);
        log.info(response);

    }

    public String sendKpiData(Map<String, Object> configDsData, Map<String, String> kpiData, String instanceName,String timestamp) {

        KPIDataSender kpiDataSender = new KPIDataSender();
        String response = null;
        for (String agentId : configDsData.keySet()) {

            HashMap<String, Object> dataToSend = new HashMap<>();
            dataToSend.put("agentId", agentId);

            HashMap<String, Object> instanceData = (HashMap<String, Object>) configDsData.get(agentId);

            log.info(instanceName);
            ArrayList<Object> instanceDataToSend = new ArrayList<>();

            if (instanceData.containsKey(instanceName)) {
                ArrayList<Object> kpiDataForCorrespondingInstance = (ArrayList<Object>) instanceData.get(instanceName);

                for (int i = 0; i < kpiDataForCorrespondingInstance.size(); i++) {

                    HashMap<String, Object> params = (HashMap<String, Object>) kpiDataForCorrespondingInstance.get(i);

                    if (kpiData.containsKey(params.get("kpiName"))) {
                        params.put("val", kpiData.get(params.get("kpiName")));
                        instanceDataToSend.add(params);
                    }
                }

                log.info("For Timestamp = "+timestamp);

                dataToSend.put(instanceName, instanceDataToSend);

                response = kpiDataSender.runKpiDataSender(instanceName, dataToSend, timestamp);
                if (response == null) {
                    return null;
                }
                response = response.trim();
//                channel.shutdown();

            }

        }
        return response;

    }


    public String runKpiDataSender(String instanceId, HashMap<String, Object> params, String startTime) {

        try {
//            String response = "Fildes";
            prepareKPIData = new PrepareKPIData();

            agentId = (String) params.get("agentId");

            kpiMessageBuilder = KPIAgentMessageProtos.KPIAgentMessage.newBuilder().setAgentUid(agentId);
            KPIAgentMessageProtos.KPIAgentMessage.Instance.Builder instanceBuilder = null;

            instanceBuilder = KPIAgentMessageProtos.KPIAgentMessage.Instance.newBuilder().setInstanceId(instanceId);

            ArrayList<Object> kpis = (ArrayList<Object>) params.get(instanceId);
            for (int i = 0; i < kpis.size(); i++) {
                KPIAgentMessageProtos.KPIAgentMessage.KpiData kpi = prepareKPIData.getKpiData(kpis.get(i), startTime);
                instanceBuilder.addKpiData(kpi);

            }

            KPIAgentMessageProtos.KPIAgentMessage.Instance instance = instanceBuilder.build();

            kpiMessageBuilder.addInstances(instance);


            KPIAgentMessageProtos.KPIAgentMessage kpiMessage = kpiMessageBuilder.build();

//            System.out.println(kpiMessage.toString());
//            String response = sendRequest(kpiMessage);
            log.info("sending this kpis to GRPC ");
            log.info(kpiMessage);
            String response = DataSender.sendKPIRequest(kpiMessage);
            return response;
        } catch (Exception e1) {
           log.error("Error ",e1);
        }

        return null;
    }

}
